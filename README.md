Deployment on Push
------

- https://docs.gitlab.com/ce/ci/
- https://docs.gitlab.com/ce/ci/yaml/README.html

Secrets management
------

- https://gitlab.com/pwesner/hge-demo/settings/ci_cd > Environment variables

Access rights management
------

- https://gitlab.com/pwesner/hge-demo/project_members
- Note: Everyone with access to CI has access to secrets. Even without ci access, the vars could be read (env > mail)

Automatic Build Test
------

- .gitlab-ci.yml

Automatic Tests
------

- .gitlab-ci.yml
